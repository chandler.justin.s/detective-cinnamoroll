var PressStart = pc.createScript('pressStart');
var root, title, game;

// initialize code called once per entity
PressStart.prototype.initialize = function() {
    root = this.app.root;
    title = root.findByName("TitleScreen");
    game = root.findByName("Game");
};

// update code called every frame
PressStart.prototype.update = function(dt) {
    if (this.app.keyboard.wasPressed(pc.KEY_ENTER))
    {
        title.enabled = false;
        game.enabled = true;
    }
};

// swap method called for script hot-reloading
// inherit your script state here
// PressStart.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/