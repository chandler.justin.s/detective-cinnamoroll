var ObstacleTrigger = pc.createScript('obstacleTrigger');

ObstacleTrigger.attributes.add('player', {
    type: 'entity',
    title: 'Player Entity',
    description: 'The player to kill'
});

ObstacleTrigger.attributes.add('trafficModel', {
    type: 'asset',
    title: 'Cone Model'
});

ObstacleTrigger.attributes.add('bananaModel', {
    type: 'asset',
    title: 'Banana Model'
});

// initialize code called once per entity
ObstacleTrigger.prototype.initialize = function() {
    this.entity.collision.on('triggerenter', this.onTriggerEnter, this);
    var random = Math.floor(Math.random()*3);
    
    if (random === 1)
        this.entity.model.asset = this.trafficModel;
    
    if (random === 2)
        this.entity.model.asset = this.bananaModel;
};

// update code called every frame
ObstacleTrigger.prototype.update = function(dt) {
    this.entity.translate(-60*dt, 0, 0);
    if(this.entity.getPosition().x <= -200)
        this.entity.destroy();
};

ObstacleTrigger.prototype.onTriggerEnter = function(entity) {
    if(entity === this.player)
        {
            this.player.script.carControl.lost = true;
        }
    
    if(entity.script.policeCarScript)
        {
            entity.script.policeCarScript.destroyed();
        }
};

// swap method called for script hot-reloading
// inherit your script state here
// ObstacleTrigger.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/