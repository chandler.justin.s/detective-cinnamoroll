var TreeScript = pc.createScript('treeScript');

TreeScript.attributes.add('Xreset', {
    type: 'number'
});

// initialize code called once per entity
TreeScript.prototype.initialize = function() {
    this.home = new pc.Vec3(0,0,0);
    this.home.x = this.entity.getPosition().x;
    this.home.y = this.entity.getPosition().y;
    this.home.z = this.entity.getPosition().z;
};

// update code called every frame
TreeScript.prototype.update = function(dt) {
    this.entity.translate(-70*dt, 0, 0);
    if (this.entity.getPosition().x <= this.Xreset)
        this.entity.setPosition(this.home.x, this.home.y, this.home.z);
};

// swap method called for script hot-reloading
// inherit your script state here
// TreeScript.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/