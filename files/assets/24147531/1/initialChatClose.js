var InitialChatClose = pc.createScript('initialChatClose');

// initialize code called once per entity
InitialChatClose.prototype.initialize = function() {
    this.chatText = this.app.root.findByName('ChatText');
    this.highway = this.app.root.findByName('Highway');
    this.credits = this.app.root.findByName('CreditsScreen');
};

// update code called every frame
InitialChatClose.prototype.update = function(dt) {
    if (this.app.keyboard.wasPressed(pc.KEY_SPACE)) {
        this.chatText.script.chatbox.changeText(' ');
        if(this.entity.name === 'PoliceCutscene')
            {
                this.entity.enabled = false;
                this.highway.enabled = true;
            }
        
        if(this.entity.name === 'BeachCutscene')
            {
                this.entity.enabled = false;
                this.credits.enabled = true;
            }
        //this.entity.s
        //this.entity.script.initialChatClose.enabled = false;
    }
};

