var FollowPlayer = pc.createScript('followPlayer');

// initialize code called once per entity
FollowPlayer.prototype.initialize = function() {
    this.followObject = this.app.root.findByName("WrestlerParent").getPosition();
    this.followParent = this.app.root.findByName("WrestlerParent");
    this.first = true;
    //this.entity.setPosition(this.followObject.x, this.followObject.y-2.5, this.followObject.z);
};

// update code called every frame
FollowPlayer.prototype.update = function(dt) {
    if(this.first === true)
        {
            this.first = false;
            this.entity.setPosition(this.followObject.x, this.followObject.y-2.5, this.followObject.z);
            this.entity.reparent(this.followParent);
        }
};

// swap method called for script hot-reloading
// inherit your script state here
// FollowPlayer.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/