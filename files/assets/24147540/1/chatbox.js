var Chatbox = pc.createScript('chatbox');

// initialize code called once per entity
Chatbox.prototype.initialize = function() {
    
    this.initialText = 'Your breakfast has gone missing! Search around town for your missing bacon, eggs, and coffee and get to the bottom of this mystery!';
    
    this.changeText(this.initialText);
    this.chatImage = this.app.root.findByName('ChatImage');
    
    if(this.entity.getParent().getParent().getParent().getParent().name === 'PoliceCutscene')
        this.changeText('Uh oh. It looks like your breakfast items are actually from the black market! Now that you think about it, you don\'t even remember making breakfast this morning. And maybe trusting all of those guys in trench coats wasn\'t such a good idea. You gotta get out of here!');
    
    if(this.entity.name === 'BeachChatText')
        this.changeText('You did it! You escaped the police and totally trashed your career as a detective, but you have to admit, that WAS some pretty good coffee. Now there\'s nothing left to do but kick back with some lemonade and celebrate a job well done. And assume a fake identity.');
    
};

// update code called every frame
Chatbox.prototype.update = function(dt) {
    
    if (this.displayText === ' '){
        this.chatImage.enabled = false;
    }
    
    else {
        this.chatImage.enabled = true;
    }
    
};

Chatbox.prototype.changeText = function(newText) {
    this.displayText = newText;
    this.entity.element.text = newText;
};

// swap method called for script hot-reloading
// inherit your script state here
// Chatbox.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/

