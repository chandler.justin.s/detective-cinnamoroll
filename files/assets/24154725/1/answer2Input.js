var Answer2Input = pc.createScript('answer2Input');

// initialize code called once per entity
Answer2Input.prototype.initialize = function() {
    this.entity.element.text = '';
    this.gameScene = this.app.root.findByName('Game');
    this.riddleScene = this.app.root.findByName('SecondRiddle');
    this.director = this.app.root.findByName('Director');
    this.chatText = this.app.root.findByName('ChatText');
    this.done = false;
};

// update code called every frame
Answer2Input.prototype.update = function(dt) {
    
    if(this.entity.element.text === 'four' && this.app.keyboard.wasPressed(pc.KEY_ENTER))
        {
            this.done = true;
            this.entity.element.text = 'You got it!';
            setTimeout(function () {pc.app.root.findByName('RiddleTwoText').script.answer2Input.backToGame();}, 3000);
        }
    
    if(this.done === true)
        return;
    
    if(this.entity.element.text !== 'four' && this.app.keyboard.wasPressed(pc.KEY_ENTER))
        {
            this.entity.element.text = 'Try again!';
        }
    
    if(this.entity.element.text !== '' && this.app.keyboard.wasPressed(pc.KEY_BACKSPACE) && this.done === false)
        this.entity.element.text = '';
    if(this.app.keyboard.wasPressed(pc.KEY_A))
        this.entity.element.text += 'a';
    if(this.app.keyboard.wasPressed(pc.KEY_B))
        this.entity.element.text += 'b';
    if(this.app.keyboard.wasPressed(pc.KEY_C))
        this.entity.element.text += 'c';
    if(this.app.keyboard.wasPressed(pc.KEY_D))
        this.entity.element.text += 'd';
    if(this.app.keyboard.wasPressed(pc.KEY_E))
        this.entity.element.text += 'e';
    if(this.app.keyboard.wasPressed(pc.KEY_F))
        this.entity.element.text += 'f';
    if(this.app.keyboard.wasPressed(pc.KEY_G))
        this.entity.element.text += 'g';
    if(this.app.keyboard.wasPressed(pc.KEY_H))
        this.entity.element.text += 'h';
    if(this.app.keyboard.wasPressed(pc.KEY_I))
        this.entity.element.text += 'i';
    if(this.app.keyboard.wasPressed(pc.KEY_J))
        this.entity.element.text += 'j';
    if(this.app.keyboard.wasPressed(pc.KEY_K))
        this.entity.element.text += 'k';
    if(this.app.keyboard.wasPressed(pc.KEY_L))
        this.entity.element.text += 'l';
    if(this.app.keyboard.wasPressed(pc.KEY_M))
        this.entity.element.text += 'm';
    if(this.app.keyboard.wasPressed(pc.KEY_N))
        this.entity.element.text += 'n';
    if(this.app.keyboard.wasPressed(pc.KEY_O))
        this.entity.element.text += 'o';
    if(this.app.keyboard.wasPressed(pc.KEY_P))
        this.entity.element.text += 'p';
    if(this.app.keyboard.wasPressed(pc.KEY_Q))
        this.entity.element.text += 'q';
    if(this.app.keyboard.wasPressed(pc.KEY_R))
        this.entity.element.text += 'r';
    if(this.app.keyboard.wasPressed(pc.KEY_S))
        this.entity.element.text += 's';
    if(this.app.keyboard.wasPressed(pc.KEY_T))
        this.entity.element.text += 't';
    if(this.app.keyboard.wasPressed(pc.KEY_U))
        this.entity.element.text += 'u';
    if(this.app.keyboard.wasPressed(pc.KEY_V))
        this.entity.element.text += 'v';
    if(this.app.keyboard.wasPressed(pc.KEY_W))
        this.entity.element.text += 'w';
    if(this.app.keyboard.wasPressed(pc.KEY_X))
        this.entity.element.text += 'x';
    if(this.app.keyboard.wasPressed(pc.KEY_Y))
        this.entity.element.text += 'y';
    if(this.app.keyboard.wasPressed(pc.KEY_Z))
        this.entity.element.text += 'z';
};

Answer2Input.prototype.backToGame = function()
{
    this.riddleScene.enabled = false;
    this.director.script.globalVars.riddle2 = true;
    this.gameScene.enabled = true;
    this.chatText.script.chatbox.changeText('You got your scrambled eggs!');
    if (this.director.script.globalVars.riddle1 && this.director.script.globalVars.riddle2 && this.director.script.globalVars.riddle3)
        this.chatText.script.chatbox.changeText('You got your scrambled eggs! Looks like that\'s everything! Let\'s head back to the office!');
};

// swap method called for script hot-reloading
// inherit your script state here
// Answer1Input.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/