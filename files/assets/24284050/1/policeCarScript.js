var PoliceCarScript = pc.createScript('policeCarScript');

PoliceCarScript.attributes.add('displayText', {
    type: 'entity',
    title: 'Display Text'
});

// initialize code called once per entity
PoliceCarScript.prototype.initialize = function() {
    this.lost = false;
    this.targetX = this.entity.children[0].name;
    this.invulnerable = true;
};

// update code called every frame
PoliceCarScript.prototype.update = function(dt) {
    if(this.lost && this.entity.getPosition().x <= -220)
        this.entity.destroy();
    
    if(this.lost)
        {
            this.entity.translate(-80*dt, 80*dt, 0);
            this.entity.rotate(0,0,800*dt);
            return;
        }
    
    if(this.entity.getPosition().x <= -76)
        {
            this.entity.translate(60*dt, 0, 0);
        }
    
    if(this.entity.getPosition().x <= -96)
        {
            this.invulnerable = true;
            return;
        }
    
    this.invulnerable = false;    
};

PoliceCarScript.prototype.destroyed =  function ()
{
    if(this.invulnerable)
        return;
    
    if(this.lost === true)
        return;
    
    this.entity.sound.play('Slot 1');
    
    if(this.displayText.element.text === 'Road Condition: EXTREME')
    {
        this.lost = true;
        return;
    }
    
    var replacement = this.entity.clone();
    replacement.enabled = false;
    this.entity.getParent().addChild(replacement);
    
    replacement.setPosition(this.entity.getPosition().x - 500, this.entity.getPosition().y, this.entity.getPosition().z);
    
    replacement.enabled = true;
    
    this.lost = true;
};

// swap method called for script hot-reloading
// inherit your script state here                  
// PoliceCarScript.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/