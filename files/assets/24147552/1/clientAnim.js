var ClientAnim = pc.createScript('clientAnim');

// initialize code called once per entity
ClientAnim.prototype.initialize = function() {
    this.animState = "wrestler_idle.json";
    this.lastAnimState = "wrestler_idle.json";
    this.modelEnt = this.entity.findByName("other_model");
};

// update code called every frame
ClientAnim.prototype.update = function(dt) {
    
    if (this.animState != this.lastAnimState){
        this.lastAnimState = this.animState;
        this.modelEnt.animation.play(this.animState, 0.2); // The magic number here is the animation blend speed
    }
};

// swap method called for script hot-reloading
// inherit your script state here
// ClientAnim.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/