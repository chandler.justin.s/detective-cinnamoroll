var CreditScroll = pc.createScript('creditScroll');

// initialize code called once per entity
CreditScroll.prototype.initialize = function() {
    this.waitToStart = 0;
};

// update code called every frame
CreditScroll.prototype.update = function(dt) {
    this.waitToStart += dt*1000;
    if (this.waitToStart < 3000)
        return;
    
    
    this.entity.translate(0,0.13*dt,0);
};

// swap method called for script hot-reloading
// inherit your script state here
// CreditScroll.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/