var CameraCollision = pc.createScript('cameraCollision');

// initialize code called once per entity
CameraCollision.prototype.initialize = function() {
    
};

// swap method called for script hot-reloading
// inherit your script state here
// CameraCollision.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/