var ZoomOutCamera = pc.createScript('zoomOutCamera');

// initialize code called once per entity
ZoomOutCamera.prototype.initialize = function() {
    this.time = 0;
};

// update code called every frame
ZoomOutCamera.prototype.update = function(dt) {
    this.time += dt*1000;
    
    if(this.time >= 3000 && this.time <= 6000)
        this.entity.translateLocal(0, 0, 7*dt);
};

// swap method called for script hot-reloading
// inherit your script state here
// ZoomOutCamera.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/