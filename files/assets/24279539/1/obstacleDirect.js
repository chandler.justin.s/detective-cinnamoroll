var ObstacleDirect = pc.createScript('obstacleDirect');

ObstacleDirect.attributes.add('displayText', {
    type: 'entity',
    title: 'Display Text'
});

ObstacleDirect.attributes.add('currScene', {
    type: 'entity',
    title: 'Highway Entity'
});

ObstacleDirect.attributes.add('nextScene', {
    type: 'entity',
    title: 'Next Entity'
});

// initialize code called once per entity
ObstacleDirect.prototype.initialize = function() {
    this.timer = 0;
    this.lane = 0;
    this.obstacle = 0;
    this.globalTimer = 0;
    this.timerAct = 4000;
    
    
    this.tire1 = this.entity.children[0];
    this.tire2 = this.entity.children[1];
    this.tire3 = this.entity.children[2];
    this.tire4 = this.entity.children[3];
    this.tire5 = this.entity.children[4];
    
};

// update code called every frame
ObstacleDirect.prototype.update = function(dt) {
    if(this.timer >= this.timerAct)
        {
            this.timer = 0;
            this.spawnObstacle();
        }
    
    if(this.globalTimer >= 10000)
    {
        this.timerAct = 1800;
        this.displayText.element.text = 'Road Condition: Moderate';
    }
    
    if(this.globalTimer >= 30000)
    {
        this.timerAct = 1000;
        this.displayText.element.text = 'Road Condition: Hazardous';
    }
    
    if(this.globalTimer >= 65000)
    {
        this.timerAct = 600;
        this.displayText.element.text = 'Road Condition: Dangerous';
    }
    
    if(this.globalTimer >= 86000)
    {
        this.timerAct = 450;
        this.displayText.element.text = 'Road Condition: EXTREME';
    }
    
    if(this.globalTimer >= 106000)
    {
        this.timerAct = 99999999;
        this.displayText.element.text = 'Road Condition: EXTREME';
    }
    
    if(this.globalTimer >= 120000)
    {
        this.currScene.enabled = false;
        this.nextScene.enabled = true;
    }
    
    this.timer += dt*1000;
    this.globalTimer += dt*1000;
}; 

ObstacleDirect.prototype.spawnObstacle = function() {
    this.lane = Math.floor(Math.random()*5);
    
    var currObst;
    
    switch(this.lane)
        {
            case 0:
                currObst = this.tire1.clone();
                break;
            
            case 1:
                currObst = this.tire2.clone();
                break;
                
            case 2:
                currObst = this.tire3.clone();
                break;
                
            case 3:
                currObst = this.tire4.clone();
                break;
                
            case 4:
                currObst = this.tire5.clone();
                break;
        }
    
    this.tire1.getParent().addChild(currObst);
    currObst.enabled = true;
    
};

// swap method called for script hot-reloading
// inherit your script state here
// ObstacleDirect.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/