var CarTurn = pc.createScript('carTurn');

// initialize code called once per entity
CarTurn.prototype.initialize = function() {
    
};

// update code called every frame
CarTurn.prototype.update = function(dt) {
    var turn = 0;
    var app = this.app;

    if (app.keyboard.isPressed(pc.KEY_W)) {
        turn -= 1;
    }

    if (app.keyboard.isPressed(pc.KEY_S)) {
        turn += 1;
    }
    
    switch(turn){
        case 0:
            this.entity.setLocalEulerAngles(0,0,0);
            break;
        case 1:
            this.entity.setLocalEulerAngles(0,-5,0);
            break;
        case -1:
            this.entity.setLocalEulerAngles(0,5,0);
            break;
    }
    
    
};

// swap method called for script hot-reloading
// inherit your script state here
// CarTurn.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/