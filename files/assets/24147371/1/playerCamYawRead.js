var PlayerCamYawRead = pc.createScript('playerCamYawRead');

// initialize code called once per entity
PlayerCamYawRead.prototype.initialize = function() {
    this._yaw = 0;
};

// update code called every frame
PlayerCamYawRead.prototype.update = function(dt) {
    this.entity.setLocalEulerAngles(0,this._yaw,0);
};

// swap method called for script hot-reloading
// inherit your script state here
// PlayerCamYawRead.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/