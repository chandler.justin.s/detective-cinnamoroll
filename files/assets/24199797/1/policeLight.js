var PoliceLight = pc.createScript('policeLight');

PoliceLight.attributes.add('materialAsset1', {
    type: 'asset'
});

PoliceLight.attributes.add('materialAsset2', {
    type: 'asset'
});

// initialize code called once per entity
PoliceLight.prototype.initialize = function() {
    
    if (this.materialAsset1) {
        this.material1 = this.materialAsset1.resource;
    } 
    
    if (this.materialAsset2) {
        this.material2 = this.materialAsset2.resource;
    } 
    
    this.currLight = true;
    this.timerr = 0.1;
};
    
// update code called every frame
PoliceLight.prototype.update = function(dt) {
  /*  this.currLight = !this.currLight;
    
    if (this.currLight)
        {
            this.material1.emissiveIntensity = 10.0;
            this.material2.emissiveIntensity = 0.0;
        }
    else
        {
            this.material1.emissiveIntensity = 0.0;
            this.material2.emissiveIntensity = 10.0;
        }*/
    
    
    this.timerr += dt*1000;
    
    if(this.timerr >= 500)
        {
            this.timerr = 0.1;
            this.lightSwap();
        }
    
};

PoliceLight.prototype.lightSwap = function() {
    this.currLight = !this.currLight;
    
    if (this.currLight)
        {
            this.material1.emissiveIntensity = 10.0;
            this.material2.emissiveIntensity = 0.0;
        }
    else
        {
            this.material1.emissiveIntensity = 0.0;
            this.material2.emissiveIntensity = 10.0;
        }
    
    this.material1.update();
    this.material2.update();
};

// swap method called for script hot-reloading
// inherit your script state here
// PoliceLight.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/