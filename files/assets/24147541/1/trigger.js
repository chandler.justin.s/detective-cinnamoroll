var Trigger = pc.createScript('trigger');

Trigger.attributes.add('target', {
    type: 'entity',
    title: 'Target Scene',
    description: 'The new scene to go to'
});

// initialize code called once per entity
Trigger.prototype.initialize = function() {
    this.entity.collision.on('triggerenter', this.onTriggerEnter, this);
    this.gameScene = this.app.root.findByName('Game');
    this.director = this.app.root.findByName('Director');
    this.player = this.app.root.findByName("WrestlerParent");
};

Trigger.prototype.onTriggerEnter = function(entity) {
    if(this.director.script.globalVars.riddle1 === false && entity === this.player && this.target.name === 'FirstRiddle')
        {
            this.gameScene.enabled = false;   
            this.target.enabled = true;
        }
    
    if(this.director.script.globalVars.riddle2 === false && entity === this.player && this.target.name === 'SecondRiddle')
        {
            this.gameScene.enabled = false;   
            this.target.enabled = true;
        }
    
   if(this.director.script.globalVars.riddle3 === false && entity === this.player && this.target.name === 'ThirdRiddle')
        {
            this.gameScene.enabled = false;   
            this.target.enabled = true;
        }
    
   if(this.director.script.globalVars.riddle3 === true && this.director.script.globalVars.riddle2 === true && this.director.script.globalVars.riddle1 === true && entity === this.player && this.target.name === 'PoliceCutscene')
        {
            this.app.root.findByName("Root").script.network.socket.disconnect();
            var clone = this.target.clone();
            this.target.getParent().addChild(clone);
            this.gameScene.enabled = false;   
            clone.enabled = true;
        }
};