var GlobalVars = pc.createScript('globalVars');

// initialize code called once per entity
GlobalVars.prototype.initialize = function() {
    this.riddle1 = false;
    this.riddle2 = false;
    this.riddle3 = false;
};

// update code called every frame
GlobalVars.prototype.update = function(dt) {
    
};

// swap method called for script hot-reloading
// inherit your script state here
// GlobalVars.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/