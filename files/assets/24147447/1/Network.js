var Network = pc.createScript('network');

// initialize code called once per entity
Network.prototype.initialize = function() {
    this.socket = io.connect('https://charm-spaghetti.glitch.me');
    
    this.myAnimState = "wrestler_idle.json";
    
    this.socket.emit ('initialize');
    var socket = this.socket;
    
    socket.sendBuffer.length = 1;
    
    this.shouldSend = 0;

    this.player = this.app.root.findByName ('WrestlerParent');
    this.playerModel = this.app.root.findByName ('WrestlerOffset');
    this.other = this.app.root.findByName ('other');

    var self = this;
    socket.on ('playerData', function (data) {
        self.initializePlayers (data);
    });

    socket.on ('playerJoined', function (data) {
        self.addPlayer (data);
    });
    
    socket.on ('playerMoved', function (data) {
        self.movePlayer (data);
    });
    
    socket.on ('playerAnimed', function (data) {
        self.clientAnimation (data);
    });
    
    socket.on ('playerDisconnected', function (data) {
        self.deletePlayer (data);
    });
    
};

// update code called every frame
Network.prototype.update = function (dt) {
    //this.updatePosition();
    
    for(var element in this.players) {
        var calcX = (this.players[element].entity.getPosition().x*3 + this.players[element].x);
        var calcY = (this.players[element].entity.getPosition().y*3 + this.players[element].y); 
        var calcZ = (this.players[element].entity.getPosition().z*3 + this.players[element].z);
        
        var calcRot = new pc.Quat(this.players[element].rx, this.players[element].ry, this.players[element].rz, this.players[element].rw);
        
        this.players[element].entity.rigidbody.teleport (calcX/4, calcY/4, calcZ/4);
        this.players[element].entity.setRotation (this.rotateTowards(this.players[element].entity.getRotation(), calcRot, 0.25));
    }
    
    this.shouldSend += 1;
    if (this.shouldSend === 1)
        this.updatePosition ();
    if (this.shouldSend === 4)
        this.shouldSend = 0;
};

Network.prototype.initializePlayers = function (data) {
    this.players = data.players;
    // Create a player array and populate it with the currently connected players.

    this.id = data.id;
    // Keep track of what ID number you are.

    for(var id in this.players){
        if(id != Network.id){
            this.players[id].entity = this.createPlayerEntity(this.players[id]);
        }
    }
    // For every player already connected, create a new capsule entity.

    this.initialized = true;
    // Mark that the client has received data from the server.
};

Network.prototype.createPlayerEntity = function (data) {
    var newPlayer = this.other.clone ();
    // Create a new player entity.

    newPlayer.enabled = true;
    // Enable the newly created player.

    this.other.getParent ().addChild (newPlayer);
    // Add the entity to the entity hierarchy.

    if (data)
        newPlayer.rigidbody.teleport (data.x, data.y, data.z);
    // If a location was given, teleport the new entity to the position of the connected player.

    return newPlayer;
    // Return the new entity.
};

Network.prototype.addPlayer = function (data) {
    if (!this.players[data.id]){
        this.players[data.id] = data;
        this.players[data.id].entity = this.createPlayerEntity(data);

        this.players[data.id].x = this.players[data.id].entity.getPosition().x;
        this.players[data.id].y = this.players[data.id].entity.getPosition().y;
        this.players[data.id].z = this.players[data.id].entity.getPosition().z;
        this.players[data.id].rw = this.players[data.id].entity.getRotation().w;
        this.players[data.id].rx = this.players[data.id].entity.getRotation().x;
        this.players[data.id].ry = this.players[data.id].entity.getRotation().y;
        this.players[data.id].rz = this.players[data.id].entity.getRotation().z;

        this.players[data.id].cAnim = "wrestler_idle.json";
    }
};

Network.prototype.movePlayer = function (data) {
    if (this.initialized) {
        this.players[data.id].x = data.x;
        this.players[data.id].y = data.y;
        this.players[data.id].z = data.z;
        this.players[data.id].rw = data.rw;
        this.players[data.id].rx = data.rx;
        this.players[data.id].ry = data.ry;
        this.players[data.id].rz = data.rz;
        this.players[data.id].entity.script.clientAnim.animState = data.cAnim;
        //this.players[data.id].entity.rigidbody.teleport (this.players[data.id].x, this.players[data.id].y, this.players[data.id].z);
        //this.players[data.id].entity.setLocalEulerAngles(data.rx, data.ry, data.rz);
    }
};

Network.prototype.clientAnimation = function (data) {
    if (this.initialized)
        this.players[data.id].entity.script.clientAnim.animState = data.animState;
};

Network.prototype.deletePlayer = function (data) {
    if(this.players[data] === 'undedfined')
        return;
    if(typeof this.players[data].entity === 'undefined')
        return;
    if (this.initialized){
        this.players[data].entity.destroy();
        delete this.players[data];
    }
};

Network.prototype.updatePosition = function () {
    if (this.initialized) {
        var pos = this.player.getPosition ();
        var rot = this.playerModel.getRotation ();
        var anim = this.myAnimState;
        this.socket.emit ('positionUpdate', {id: this.id, x: pos.x, y: pos.y, z: pos.z, rw: rot.w, rx: rot.x, ry: rot.y, rz: rot.z, cAnim: anim});
    }
};

Network.prototype.updateAnimation = function (newAnim) {
    if (this.initialized) {
        //this.socket.emit ('animationUpdate', {id: this.id, animState: newAnim});
        this.myAnimState = newAnim;
    }
};

// Get the dot product between two quaternions
Network.prototype.dot = function (quat_left, quat_right) {
    var dot = quat_left.x * quat_right.x + quat_left.y * quat_right.y + 
        quat_left.z * quat_right.z + quat_left.w * quat_right.w;

    return dot;
};

// Returns the angle in degrees between two rotations /a/ and /b/.
Network.prototype.quatAngle = function (quat_a, quat_b) {
    var dot = this.dot(quat_a, quat_b);
    
    if(quat_a.equals(quat_b) )
    {
        return 0;
    }        
    
    var rad2Deg = 1 / (Math.PI / 180);

    var angle = Math.acos(Math.min(Math.abs(dot), 1)) * 2 * rad2Deg;

    return angle;
    
};

Network.prototype.rotateTowards = function (quat_a, quat_b, percent) {
    var angle = this.quatAngle(quat_a, quat_b);
        
    if (angle === 0) //Check against our base case
    {
        return quat_b;
    }
    
    return new pc.Quat().slerp(quat_a, quat_b, percent); //Return the interpolated result
    
};