var GroundCheck = pc.createScript('groundCheck');

// initialize code called once per entity
GroundCheck.prototype.initialize = function() {
    
};

// update code called every frame
GroundCheck.prototype.update = function(dt) {
    
};

// swap method called for script hot-reloading
// inherit your script state here
// GroundCheck.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/