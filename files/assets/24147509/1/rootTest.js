var RootTest = pc.createScript('rootTest');

// initialize code called once per entity
RootTest.prototype.initialize = function() {
    
};

// update code called every frame
RootTest.prototype.update = function(dt) {
    
};

RootTest.prototype.callMe = function() {
    
};

// swap method called for script hot-reloading
// inherit your script state here
// RootTest.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/