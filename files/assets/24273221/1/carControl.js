var CarControl = pc.createScript('carControl');

// initialize code called once per entity
CarControl.prototype.initialize = function() {
    this.force = new pc.Vec3(); // Our running total force that we are adding to and applying every frame
    this.subforce = new pc.Vec3(); // The force desired to be added to the running total based on the keyboard input
    this.finalForce = new pc.Vec3(); // The temporary property used for calculations before replacing the force variable
    
    this.forceMultiplier = 300;
    
    this.resetPosition = this.entity.getPosition();
    this.resetRotation = this.entity.getRotation();
    this.lost = false;
    this.sounded = false;
    
    this.entity.getParent().enabled = false;
    this.saveState = this.entity.getParent().clone();
    this.entity.getParent().enabled = true;
    this.entity.getParent().getParent().addChild(this.saveState);
    
};

// update code called every frame
CarControl.prototype.update = function(dt) {

    var turn = 0;
    var app = this.app;
    
    if(this.lost)
        {
            if(!this.sounded)
                {
                    this.sounded = true;
                    this.entity.sound.play('Slot 1');
                }
            this.entity.rigidbody.enabled = false;
            this.entity.translate(-80*dt, 80*dt, 0);
            this.entity.rotate(0,0,800*dt);
            if(this.entity.getPosition().y >= 200)
                {
                    this.entity.getParent().destroy();
                    this.saveState.enabled = true;
                }
            return;
        }
    
    
    // Movement axis variables
    var x = 0;
    var z = 0;

    // Use W-A-S-D keys to move player
    // Check for key presses
    if (app.keyboard.isPressed(pc.KEY_A)) { // Move left when the user presses A. Note that we must always calculate both axes to account for any rotations the artist has done.
        x += this.entity.forward.x;
        z += this.entity.forward.z;
    }

    if (app.keyboard.isPressed(pc.KEY_D)) {
        x -= this.entity.forward.x;
        z -= this.entity.forward.z;
    }

    if (app.keyboard.isPressed(pc.KEY_W)) {
        x += this.entity.right.x;
        z += this.entity.right.z;
        turn -= 1;
    }

    if (app.keyboard.isPressed(pc.KEY_S)) {
        x -= this.entity.right.x;
        z -= this.entity.right.z;
        turn += 1;
    }
    
    /*switch(turn){
        case 0:
            this.entity.setLocalEulerAngles(0,90,0);
            break;
        case 1:
            this.entity.setLocalEulerAngles(0,135,0);
            break;
        case -1:
            this.entity.setLocalEulerAngles(0,45,0);
            break;
    }*/
    
    // Use the direction from keypresses to apply force to the character
    if (x !== 0 || z !== 0) {
        
        this.force.set(x, 0, z).normalize().scale(this.forceMultiplier); // Normalize the force and make it a reasonable amount
        this.finalForce.set(this.force.x - this.entity.rigidbody.linearVelocity.x, 0, this.force.z - this.entity.rigidbody.linearVelocity.z); // Calculate the force in each axis
        this.entity.rigidbody.applyForce(this.finalForce); // Apply the force calculated
    }
    
};

// swap method called for script hot-reloading
// inherit your script state here
// CarControl.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/