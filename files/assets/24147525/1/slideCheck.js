var SlideCheck = pc.createScript('slideCheck');

// initialize code called once per entity
SlideCheck.prototype.initialize = function() {
    this.entity.collision.on('triggerenter', this.onTriggerEnter, this);
    this.entity.collision.on('triggerleave', this.onTriggerExit, this);
};

// update code called every frame
SlideCheck.prototype.update = function(dt) {
    
};


SlideCheck.prototype.onTriggerEnter = function (otherEntity) {
    // it is not teleportable
    if (! otherEntity.script.teleportable)
        return;

    // teleport entity to the target entity
    otherEntity.script.wrestlerMovement.onSlide = true;
};

SlideCheck.prototype.onTriggerExit = function (otherEntity) {
    // it is not teleportable
    if (! otherEntity.script.teleportable)
        return;

    // teleport entity to the target entity
    otherEntity.script.wrestlerMovement.onSlide = false;
};

// swap method called for script hot-reloading
// inherit your script state here
// SlideCheck.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/