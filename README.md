Project playable at: https://consistency.itch.io/detective-cinnamoroll

For the JavaScript that I wrote, see the scripts folder.

*Glitch did a project purge, so this game is no longer online and is instead a singleplayer experience.*

Help detective Cinnamoroll chug coffee and find his missing breakfast!

If you get stuck on the riddles and would like to move on to the rest of the game, you can find the answers here: https://pastebin.com/KHcKNKDN

Please note that all riddle solutions are a single noun, no articles necessary, and spelled exclusively with letters, not numbers.

While running around town and solving the riddles, you will be able to see other Cinnamoroll players doing the same!

A Sanrio fan game made in 2 weeks for the Buddy Jam along with my twin brother.

For the most optimized experience, play using Chrome.